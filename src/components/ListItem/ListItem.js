import React from 'react';
import {Modal, View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';


const listItem = (props) => (
    <TouchableOpacity onPress={props.onItemPressed}>
        <View style={styles.listItem}>
            <Image resizeMode="contain" source={props.placeImage} style={styles.listItemImage}/>
            <Text style={styles.listItemText}>
                {props.placeName}
            </Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    listItem: {
        width: '100%',
        padding: 10,
        backgroundColor: '#eee',
        borderBottomWidth: 1,
        borderBottomColor: '#1abc9c',
        marginBottom: 2,
        flexDirection: 'row',
        alignItems: 'center'
    },
    listItemText: {
        color: '#1abc9c',
        fontWeight: 'bold',
    },
    listItemImage: {
        width: 30,
        height: 20,
        marginRight: 8
    }
});

export default listItem;


 