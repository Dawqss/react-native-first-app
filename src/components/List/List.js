import React from "react";
import { FlatList, StyleSheet } from "react-native";
import ListItem from "../ListItem/ListItem";

const list = props => {
  return (
    <FlatList
      style={styles.itemListContainer}
      data={props.places}
      renderItem={(element) => ( 
        <ListItem
          key={element.item.id}
          placeName={element.item.name}
          placeImage={element.item.placeImage}
          onItemPressed={() => props.onItemSelected(element.item.key)}
        />
      )}
    >      
    </FlatList>
  );
};

const styles = StyleSheet.create({
  itemListContainer: {
    width: "100%"
  }
});

export default list;
