import React, { Component } from 'react';
import {View, Button, TextInput, StyleSheet} from 'react-native';

class InputBar extends Component {
  state = {
    placeName: ''
  };

  placeNameChangedHandler = val => {
    this.setState({
      placeName: val
    })
  };

  onPressLearnMore = val => {
    if (this.state.placeName.trim() === '') {
      return;
    } 

    this.props.onPlaceAdded(this.state.placeName);
    this.setState({
      placeName: ''
    })
  };

  render() {
    return (
      <View style={styles.topBar}>
        <TextInput  
          style={styles.inputBar}
          value={this.state.placeName}
          placeholder="An awesome place"
          onChangeText={this.placeNameChangedHandler}/>
          <Button
            onPress={this.onPressLearnMore}
            title="Add"
            color="#1abc9c"
            style={this.button}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  topBar: {
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  inputBar: {
    height: 40,    
    flex: 70,    
  },
  
});

export default InputBar;