import React from 'react';
import {Text, StyleSheet} from 'react-native';

const textHeading = props => {
    return (
        <Text
            {...props}
            style={[styles.header, props.styles]}>
            {props.children}
        </Text>
    );
};

const styles = StyleSheet.create({
    header: {
        fontSize: 25,
        fontWeight: '500'
    }
});

export default textHeading;