import React from 'react';
import { TextInput, StyleSheet } from 'react-native';

const defaultInput = props => (
    <TextInput
        underlineColorAndroid="transparent"
        {...props}
        style={[styles.input, props.style]}/>
);

const styles = StyleSheet.create({
    input: {
        width: '100%',
        borderWidth: 1,
        padding: 5,
        borderColor: '#eee',
        marginBottom: 15,
        borderRadius: 3
    }
});


export default defaultInput;