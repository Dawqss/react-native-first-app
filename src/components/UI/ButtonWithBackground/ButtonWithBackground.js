import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';

const buttonWithBackground = props => (
    <TouchableOpacity onPress={props.onPress}>
        <View style={[styles.button, {backgroundColor: props.backgroundColor}]}>
            <Text style={styles.text}>{props.children}</Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    button: {
        padding: 10,
        margin: 5,
        borderRadius: 2
    },
    text: {
        color: 'black'
    }
});

export default buttonWithBackground;