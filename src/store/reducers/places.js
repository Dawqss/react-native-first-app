import { ADD_PLACE, DELETE_PLACE } from '../actions/actionTypes'

const initalState = {
    places: [],
    selectedPlace: null
};

const placesReducer = (state = initalState, action) => {
    switch (action.type) {
        case ADD_PLACE:
            return {
                ...state,
                places: state.places.concat({
                    key: Math.random().toString(),
                    name: action.placeName,
                    placeImage: {
                        uri: 'https://www.dccomics.com/sites/default/files/GalleryChar_1920x1080_Hush_54b5d1be8e4ca8.68343525.jpg'
                    }
                })
            };
        case DELETE_PLACE: {
            return {
                ...state,
                places: state.places.filter(place => {
                    return place.key !== action.placeKey
                }),
                selectedPlace: null
            }
        }
        default:
            return state;
    }
};

export default placesReducer;