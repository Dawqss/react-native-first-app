import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const startTabs = () => {
    Promise.all([
        Icon.getImageSource('ios-search', 30),
        Icon.getImageSource('ios-share-alt', 30),
        Icon.getImageSource('ios-menu', 30),
    ]).then((sources) => {
        Navigation.startTabBasedApp({
            tabs: [
                {
                    screen: 'first-app.FindPlacesScreen',
                    title: 'Find Place',
                    label: 'Find Place',
                    icon: sources[0],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[2],
                                title: 'Menu',
                                id: 'navMenuToggle'
                            }
                        ]
                    }
                },
                {
                    screen: 'first-app.SharePlacesScreen',
                    title: 'Share Place',
                    label: 'Share Place',
                    icon: sources[1],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[2],
                                title: 'Menu',
                                id: 'navMenuToggle'
                            }
                        ]
                    }
                }
            ],
            drawer: {
                left: {
                    screen: 'first-app.SideDrawer'
                }
            }
        });
    });
};

export default startTabs;