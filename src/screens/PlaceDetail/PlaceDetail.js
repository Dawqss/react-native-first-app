import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { deletePlace } from "../../store/actions";



class PlaceDetailScreen extends Component {
    placeDeleteHandler = () => {
        this.props.onDeletePlace(this.props.selectedPlace.key);
        this.props.navigator.pop();
    };
    render() {
        return (
            <View style={styles.modalContainer}>
                <View>
                    <Image
                        source={this.props.selectedPlace.placeImage ? this.props.selectedPlace.placeImage : null}
                        style={styles.image}
                        resizeMode="contain"/>
                    <Text style={styles.name}>{this.props.selectedPlace.name}</Text>
                </View>
                <TouchableOpacity onPress={this.placeDeleteHandler}>
                    <View style={styles.deleteButton}>
                        <Icon size={30} name="ios-trash" color="red"/>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modalContainer: {
        margin: 22
    },
    image: {
        width: '100%',
        height: 200
    },
    name: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 28,
        margin: 10
    },
    deleteButton: {
        alignItems: 'center'
    }
});

const mapDispatchToProps = dispatch => {
    return {
        onDeletePlace: (key) => dispatch(deletePlace(key))
    }
};

export default connect(null, mapDispatchToProps)(PlaceDetailScreen);