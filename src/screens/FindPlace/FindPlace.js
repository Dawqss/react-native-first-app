import React, { Component } from 'react';
import { View, Text } from 'react-native'
import { connect } from 'react-redux';
import List from '../../components/List/List';


class FindPlaceScreen extends Component {
    onItemSelectHandler = (key) => {
        const selPlace = this.props.places.find(place => place.key === key);
        this.props.navigator.push({
            screen: 'first-app.PlaceDetailScreen',
            title: selPlace.name,
            passProps: {
                selectedPlace: selPlace
            },
            animationType: 'slide'
        })
    };

    render() {
        return (
            <View>
                <List places={this.props.places} onItemSelected={this.onItemSelectHandler}/>
            </View>
        );
    }
}

const mapStateToProps = state => {
  return {
      places: state.places.places
  }
};

export default connect(mapStateToProps)(FindPlaceScreen);