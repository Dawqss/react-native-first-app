import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import { addPlaces } from '../../store/actions/index'
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput'
import MainText from '../../components/UI/MainText/MainText';
import HeadingText from '../../components/UI/TextHeading/TextHeading';
import imagePlaceholder from '../../assets/fuji.jpg';
class SharePlaceScreen extends Component {
    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    onNavigatorEvent = event => {
        if (event.type === 'NavBarButtonPress') {
            if(event.id === 'navMenuToggle') {
                this.props.navigator.toggleDrawer({
                   side: 'left'
                });
            }
        }
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.container}>
                    <MainText style={styles.header}>
                        <HeadingText>
                            Share a place with us
                        </HeadingText>
                    </MainText>
                    <View style={styles.placeholder}>
                        <Image source={imagePlaceholder} style={styles.image}/>
                    </View>
                    <View style={styles.buttons}>
                        <Button title="Pick Image"/>
                    </View>

                    <View style={styles.placeholder}>
                        <Text>Map</Text>
                    </View>
                    <View style={styles.buttons}>
                        <Button title="Locate Me"/>
                    </View>

                    <DefaultInput placeholder="Place name"/>
                    <Button title="Share the Place!"/>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        textAlign: 'center',
        marginBottom: 16,
        marginTop: 12
    },
    placeholder: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#999999',
        width: '80%',
        marginLeft: '10%',
        height: 150
    },
    image: {
        width: '100%',
        height: '100%'
    },
    buttons: {
        margin: 8
    }
});

const mapDispatchToProps = dispatch => {
    return {
        onAddPlace: (placeName) => dispatch(addPlaces(placeName))
    }
};


export default connect(null, mapDispatchToProps)(SharePlaceScreen);