import React, { Component } from 'react';
import { View, StyleSheet, Button, ImageBackground } from 'react-native';
import startTabs from '../MainTabs/startMainTabs';
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
import TextHeading from '../../components/UI/TextHeading/TextHeading';
import MainText from '../../components/UI/MainText/MainText';
import ButtonWithBackground from '../../components/UI/ButtonWithBackground/ButtonWithBackground';

import backgroundImage from '../../assets/wallpaper.png';

class AuthScreen extends Component {
    loginHandler = () => {
        startTabs();
    };

    render() {
        return (
            <ImageBackground
                source={backgroundImage}
                style={styles.backgroundImage}>
                <View style={styles.container}>
                    <MainText style={styles.header}>
                        <TextHeading>
                            Please Log In
                        </TextHeading>
                    </MainText>
                    <ButtonWithBackground
                        backgroundColor="#03a9f4"
                        onPress={this.loginHandler}>
                            Switch to login
                    </ButtonWithBackground>
                    <View style={styles.inputContainer}>
                        <DefaultInput placeholder="Your email address" style={styles.input}/>
                        <DefaultInput placeholder="Password" style={styles.input}/>
                        <DefaultInput placeholder="Confirm password" style={styles.input}/>
                    </View>
                    <ButtonWithBackground
                        backgroundColor="#f50057"
                        onPress={this.loginHandler}>
                            Login
                    </ButtonWithBackground>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 70
    },
    inputContainer: {
        width: '100%',
        paddingLeft: 15,
        paddingRight: 15
    },
    header: {
        paddingTop: 10,
        paddingBottom: 22,
    },
    input: {
        backgroundColor: '#eee',
        borderColor: '#bbb',
        borderRadius: 3
    },
    backgroundImage: {
        width: '100%',
        flex: 1
    }
});

export default AuthScreen;