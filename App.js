import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';

import AuthScreen from './src/screens/Auth/Auth';
import SharePlaceScreen from "./src/screens/SharePlace/SharePlace";
import FindPlaceScreen from "./src/screens/FindPlace/FindPlace";
import PlaceDetailScreen from './src/screens/PlaceDetail/PlaceDetail'
import configureStore from './src/store/configureStore';
import SideDrawer from "./src/screens/sideDrawer/SideDrawer";
const store = configureStore();

//Register Screens
Navigation.registerComponent(
    "first-app.AuthScreen",
    () => AuthScreen,
    store,
    Provider
);
Navigation.registerComponent(
    "first-app.SharePlacesScreen",
    () => SharePlaceScreen,
    store,
    Provider
);
Navigation.registerComponent(
    "first-app.FindPlacesScreen",
    () => FindPlaceScreen,
    store,
    Provider
);
Navigation.registerComponent(
    "first-app.PlaceDetailScreen",
    () => PlaceDetailScreen,
    store,
    Provider
);

Navigation.registerComponent(
    "first-app.SideDrawer",
    () => SideDrawer
);

Navigation.startSingleScreenApp({
    screen: {
        screen: 'first-app.AuthScreen',
        title: 'Login'
    }
});